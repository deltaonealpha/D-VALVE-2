// Om Sri Sai Ram
/////// DEBUGGING PROGRAM - SEPERATE PROGRAM FOR BLE FUNCTIONING
// Program for DeltaOneHYDRO MARK-II
// Uses relays; Adviced to disconnect relay before code upload
// Use external relay power and 5v supply to ultrasonic - inconsistent values on 3.3v
// DO NOT remove delays
// Uses C++ native serial debugger
// Github repository - https://github.com/deltaonealpha/DeltaOneHydroMk2
// Pranav Balaji
/*_____________________ DECLERATION ________________________*/
#define relPin  7  //Relay signal pin pin 7
#define relPin2  8  //Relay signal pin pin 8
#define trigPin 10 //Trig output - digital out pin 10 
#define echoPin 11 //Echo input - digital in pin 11
/* ----------------------- Start-up code -----------------------*/
void setup()
{
  Serial.begin(9600); // For debugging purposes - C++ serial controller
  pinMode(relPin, OUTPUT); // Pin mode decleration
  pinMode(relPin2, OUTPUT); // Pin mode decleration
  pinMode(trigPin, OUTPUT); // Pin mode decleration
  pinMode(echoPin, INPUT); // Pin mode decleration
  Serial.println("Initializing now"); // To indicate start-up
  digitalWrite(relPin2, LOW); // Fresh-boot pressurzation start
  delay(2000); // Still running
  digitalWrite(relPin2, HIGH); // Presurization stop
}
/* ------------------ Looping code ----------------------*/
void loop()
{
  long duration, distance; // Variable decleration
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  duration = pulseIn(echoPin, HIGH);
  distance = (duration / 2) / 29.1; //DO NOT ALTER - formulae based operator
  Serial.println(distance);
  if (distance < 20) //Air-water boundary clause
  // DO NOT ALTER RELAY FUNCTIONS
  {
    digitalWrite(relPin, HIGH); // Primary relay - OFF
    Serial.println("-All Electronics OFF-");
  }
  else // Still filling clause
  {
    digitalWrite(relPin, LOW); // Primary relay - ON
    Serial.println("-All Electronics ON-");
  }
}

// relPin - valve
// relPin2 - pump
