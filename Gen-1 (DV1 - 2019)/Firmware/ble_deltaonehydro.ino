// Om Sri Sai Ram
/////// BLE PROGRAM - NON DEBUGGING PROGRAM
// Program for DeltaOneHYDRO MARK-II
// Uses relays; Adviced to disconnect relay before code upload
// Use external relay power and 5v supply to ultrasonic - inconsistent values on 3.3v
// DO NOT remove delays
// Uses C++ native serial debugger
// Github repository - https://github.com/deltaonealpha/DeltaOneHydroMk2
// Pranav Balaji
/*_____________________ DECLERATIONS ________________________*/
int incomingByte = 0; // for incoming serial data
#define relPin  7  //Relay signal pin pin 7
#define trigPin 10 //Trig output - digital out pin 10 
#define echoPin 11 //Echo input - digital in pin 11
#define extra5vpin 4 //Extra pin due to the shortage of VCC pins
/* ----------------------- Start-up code -----------------------*/
void setup()
{
  Serial.begin(9600); // For debugging purposes - C++ serial controller
  pinMode(relPin, OUTPUT); // Pin mode decleration
  pinMode(trigPin, OUTPUT); // Pin mode decleration
  pinMode(echoPin, INPUT); // Pin mode decleration
  pinMode(extra5vpin, OUTPUT); // Pin mode decleration

  digitalWrite(relPin, HIGH); // Primary relay - OFF
  Serial.println("Loading>>>>>>> 10 SECOND(S)");
  delay(1000);
  Serial.println("Loading>>>>>>> 9 SECOND(S)");
  delay(1000);
  Serial.println("Loading>>>>>>> 8 SECOND(S)");
  delay(1000);
  Serial.println("Loading>>>>>>> 7 SECOND(S)");
  delay(1000);
  Serial.println("Loading>>>>>>> 6 SECOND(S)");
  delay(1000);
  Serial.println("Loading>>>>>>> 5 SECOND(S)");
  delay(1000);
  Serial.println("Loading>>>>>>> 4 SECOND(S)");
  delay(1000);
  Serial.println("Loading>>>>>>> 3 SECOND(S)");
  delay(1000);
  Serial.println("Loading>>>>>>> 2 SECOND(S)");
  delay(1000);
  Serial.println("Loading>>>>>>> 1 SECOND(S)");
  delay(1000);
  Serial.println("Loading>>>>>>> 0 SECOND(S)");
  delay(1000);
  Serial.println("------- DELTAONEhydro -------"); // To indicate start-up
  delay(1000);
  Serial.println("---- pushMATRIX initializing....... ----");
  delay(1000);
  Serial.println("SYSTEM ACTIVE NOW");
}
/* ------------------ Looping code ----------------------*/
void loop()
{
  digitalWrite(extra5vpin, HIGH);
  long duration, distance; // Variable decleration
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  duration = pulseIn(echoPin, HIGH);
  distance = (duration / 2) / 29.1; //DO NOT ALTER - formulae based operator
  if (distance < 20) //Air-water boundary clause
    // DO NOT ALTER RELAY FUNCTIONS
  {
    digitalWrite(relPin, HIGH); // Primary relay - OFF
    Serial.println("Bucket seems to be filled - TAP CLOSED");
  }
  else if (Serial.available() > 0) {
    // read the incoming byte:
    incomingByte = Serial.read();
    digitalWrite(relPin, HIGH); // Primary relay - OFF
    Serial.println("MANUAL OVERRIDE - TAP CLOSED NOW");
    delay(1000);
      return;
  }

  else // Still filling clause
  {
    digitalWrite(relPin, LOW); // Primary relay - ON
    Serial.println("Bucket Filling Right Now - tip tip barsaa paani");
  }
}

// relPin - valve
// relPin2 - pump
